﻿using Cyest.Carbon.Modeller.Domain.Ids;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScaleoutComparisonForm
{
    public class Objects : Comparison
    {
        public Objects()
        {

        }

        public override void Export(string modelLocation, string destination)
        {
            using (var model = new Cyest.Carbon.Modeller.Server.Models.Model(modelLocation))
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Full Object Path");

                var objects = model.PatternManager2.FindMatchingObjects(ObjectId.Root, "**.*");

                foreach (var obj in objects)
                {
                    var ob = model.ObjectHierarchyManager.GetObjectById(obj);
                    sb.Append(ob.FullName);
                }

                File.WriteAllText(destination, sb.ToString());
            }
        }
    }
}
