﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScaleoutComparisonForm
{
    public abstract class Comparison
    {
        public string BeforeLocation { get; set; }
        public string AfterLocation { get; set; }
        public string Headers { get; set; }

        public abstract void Export(string modelLocation, string destination);

        public HashSet<string> PopulateSet(string location)
        {
            HashSet<string> set = new HashSet<string>();

            using (var reader = new StreamReader(location))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    set.Add(line);
                }
            }

            Headers = set.FirstOrDefault();

            return set;
        }

        public void ComparisonResult(HashSet<string> set, List<string> values)
        {

        }

        public void ComparisonResultOld(HashSet<string> set, List<string> values)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            DataTable table = new DataTable();

            var headers = Headers.Replace(" ", "").Split(',');
            foreach (var header in headers)
            {
                table.Columns.Add(header);
            }

            foreach (var item in set)
            {
                table.Rows.Add(item.Split(','));
            }

            var key = headers.Where(a => !values.Contains(a));

            foreach (DataRow row in table.Rows)
            {
                var rowKey = ConcatValues(key, row);
                if (dict.ContainsKey(rowKey))
                {
                    dict[rowKey] += "," + ConcatValues(values, row);
                }
                else
                {
                    dict.Add(rowKey, ConcatValues(values, row));
                }
            }

            using (var writer = new StreamWriter(@"C:\Users\NicoSaaiman\Documents\ModelComparison\before\Parameters\test.csv", true))
            {
                var newHeader = string.Join(",", key) + "," + CustomHeader(values);
                writer.WriteLine(newHeader);

                foreach (var item in dict)
                {
                    writer.WriteLine(item.Key + "," + item.Value);
                }
            }
        }

        private string CustomHeader(List<string> values)
        {
            List<string> result = new List<string>();

            foreach(var item in values)
            {
                result.Add("Before " + item );
                result.Add("After " + item );
            }

            result = result.OrderByDescending(a => a).ToList();

            return string.Join(",", result);
        }

        private string ConcatValues(IEnumerable<string> values, DataRow row)
        {
            List<string> result = new List<string>();

            foreach(var item in values)
            {
                result.Add(row[item].ToString());
            }

            return string.Join(",", result);
        }

        public HashSet<string> Compare(HashSet<string> before, HashSet<string> after)
        {
            before.SymmetricExceptWith(after);
            return before;
        }

        public void WriteToFile(string filePath, HashSet<string> set)
        {
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }

            using (var writer = new StreamWriter(filePath, true))
            {
                writer.WriteLine(Headers);

                foreach (var item in set)
                {
                    writer.WriteLine(item);                   
                }
            }
        }

        public void SetupFolders(string selectedLocation, string modelName, string type)
        {
            var mainDir = Path.Combine(selectedLocation, "ModelComparison");
            var modelDir = Path.Combine(mainDir, modelName);
            var typeDir = Path.Combine(modelDir, type);

            DirectoryExists(mainDir);
            DirectoryExists(modelDir);
            DirectoryExists(typeDir);


            BeforeLocation = typeDir;
            AfterLocation = typeDir;
        }

        private void DirectoryExists(string directory)
        {
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
        }
    }
}
