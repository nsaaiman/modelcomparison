﻿using Cyest.Carbon.Modeller.Domain.AdaptersV2.Configurations.CSV.Values;
using Cyest.Carbon.Modeller.Domain.Ids;
using Cyest.Carbon.Modeller.Domain.Templates;
using Cyest.Carbon.Modeller.Server.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScaleoutComparisonForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            DefaultLocation = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
        }

        private string DefaultLocation { get; set; }
        private string BrowseTextBox1Location { get; set; }
        private string BrowseTextBox2Location { get; set; }

       

        private string ModelName { get; set; }

        private void CompareButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(BrowseTextBox1Location) || string.IsNullOrWhiteSpace(BrowseTextBox2Location))
            {
                MessageBox.Show("Make sure both .csv files are selected!");
                return;
            }

            SetDefaults();

            Task.Run(() =>
            {
                Values values = new Values();
                values.SetupFolders(DefaultLocation, ModelName, "Values");
                values.Export(BrowseTextBox1Location, Path.Combine(values.BeforeLocation, "before.csv"));
                values.Export(BrowseTextBox2Location, Path.Combine(values.AfterLocation, "after.csv"));
                var beforeValues = values.PopulateSet(Path.Combine(values.BeforeLocation, "before.csv"));
                var afterValues = values.PopulateSet(Path.Combine(values.AfterLocation, "after.csv"));
                var resultValues = values.Compare(beforeValues, afterValues);
                //values.ComparisonResult(resultValues, new List<string>() { "Value" });
                values.WriteToFile(Path.Combine(values.BeforeLocation, "differences.csv"), resultValues);

                Objects objects = new Objects();
                objects.SetupFolders(DefaultLocation, ModelName, "Objects");
                objects.Export(BrowseTextBox1Location, Path.Combine(objects.BeforeLocation, "before.csv"));
                objects.Export(BrowseTextBox2Location, Path.Combine(objects.AfterLocation, "after.csv"));
                var beforeObjects = objects.PopulateSet(Path.Combine(objects.BeforeLocation, "before.csv"));
                var afterObjects = objects.PopulateSet(Path.Combine(objects.AfterLocation, "after.csv"));
                var resultObjects = objects.Compare(beforeObjects, afterObjects);
                objects.WriteToFile(Path.Combine(objects.BeforeLocation, "differences.csv"), resultObjects);

                Parameters parameters = new Parameters();
                parameters.SetupFolders(DefaultLocation, ModelName, "Parameters");
                parameters.Export(BrowseTextBox1Location, Path.Combine(parameters.BeforeLocation, "before.csv"));
                parameters.Export(BrowseTextBox2Location, Path.Combine(parameters.AfterLocation, "after.csv"));
                var beforeParameters = parameters.PopulateSet(Path.Combine(parameters.BeforeLocation, "before.csv"));
                var afterParameters = parameters.PopulateSet(Path.Combine(parameters.AfterLocation, "after.csv"));
                var resultParameters = parameters.Compare(beforeParameters, afterParameters);
                parameters.ComparisonResult(resultParameters, new List<string>() { "ParameterValue", "OverriddenStatus" });
                parameters.WriteToFile(Path.Combine(parameters.BeforeLocation, "differences.csv"), resultParameters);

                if (resultObjects.Any() || resultParameters.Any())
                {
                    Invoke(new MethodInvoker(delegate { ResultTextBox.Text = "Differences found!"; }));
                    Invoke(new MethodInvoker(delegate { ResultTextBox.ForeColor = Color.Red; }));
                }
                else
                {
                    Invoke(new MethodInvoker(delegate { ResultTextBox.Text = "No differences found!"; }));
                    Invoke(new MethodInvoker(delegate { ResultTextBox.ForeColor = Color.Green; }));
                }

                Invoke(new MethodInvoker(delegate { ProgressBar.Visible = false; }));
            });
        }

        private void BrowseButton1_Click(object sender, EventArgs e)
        {
            BrowseTextBox1Location = SelectModel(BrowseTextBox1);
            DefaultLocation = Path.GetDirectoryName(BrowseTextBox1Location);
            ModelName = BrowseTextBox1.Text.Split('.')[0];
        }

        private void BrowseButton2_Click(object sender, EventArgs e)
        {
            BrowseTextBox2Location = SelectModel(BrowseTextBox2);
        }

        private void SetDefaults()
        {
            ResultTextBox.Text = string.Empty;
            ProgressBar.Visible = true;
        }

        private string SelectModel(TextBox textBox)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.Filter = "cm files (*.cm)|*.cm|All files (*.*)|*.*";

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {                    
                    textBox.Text = Path.GetFileName(openFileDialog.FileName);
                    return openFileDialog.FileName;
                }
                else
                {
                    return null;
                }
            }
        }
    }
}
