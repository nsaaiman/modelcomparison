﻿using Cyest.Carbon.Modeller.Domain.AdaptersV2.Configurations.CSV.Values;
using Cyest.Carbon.Modeller.Server.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScaleoutComparisonForm
{
    public class Values : Comparison
    {   
        public Values()
        {

        }

        public override void Export(string modelLocation, string destination)
        {
            using (var model = new Cyest.Carbon.Modeller.Server.Models.Model(modelLocation))
            {
                var process = model.AdapterV2Manager().AddAdapterV2Process("CSV Comparison", true);
                CSVValueFromModelConfiguration fromConfig = new CSVValueFromModelConfiguration()
                {
                    AttributePattern = "**[*]",
                    CSVProviderConfiguration = new Cyest.Carbon.Modeller.Domain.AdaptersV2.Configurations.CSV.ToCSVProviderConfiguration()
                    {
                        CSVPath = destination,
                        Headings = true
                    },
                    ExportAttributeName = true,
                    ExportDimensionElements = true,
                    ExportObjectPathSegments = true,
                    ExportValues = true,
                    ExportDataset = false,
                    ExportTypeSegments = false,
                    ExportInstancePathSegments = false
                };

                var task = model.AdapterV2Manager().AddAdapterV2Task(process, "Values", Cyest.Carbon.Modeller.Domain.AdaptersV2.AdapterV2TaskDirection.FromModel, new CSVValueToModelConfiguration(), fromConfig, 0, true);

                var gvm = model.GlobalVariablesManager();
                model.AdapterV2Manager().RunAdapterV2(task);
            }
        }
    }
}
