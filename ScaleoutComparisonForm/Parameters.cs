﻿using Cyest.Carbon.Modeller.Domain.Templates;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScaleoutComparisonForm
{
    public class Parameters : Comparison
    {
        public Parameters()
        {

        }

        public override void Export(string modelLocation, string destination)
        {
            using (var model = new Cyest.Carbon.Modeller.Server.Models.Model(modelLocation))
            {
                StringBuilder sb2 = new StringBuilder();

                sb2.Append("Instance Path, Parameter Name, Parameter Value, Parameter Type, Overridden Status");
                sb2.AppendLine();
                foreach (var templateModel in model.Template2Manager.GetTemplateModels())
                {
                    var instances = model.Template2Manager.FindInstances(templateModel.Definition.TemplateId);

                    foreach (var instance in instances)
                    {
                        var tempValues = model.Template2Manager.GetTemplateValues(instance);
                        foreach (var paramValue in tempValues.ParameterValues)
                        {
                            var paramValType = paramValue.Value.GetType();
                            if (paramValType == typeof(ObjectListParameterValue))
                                continue;

                            var instanceObj = model.ObjectHierarchyManager.GetObjectById(instance);
                            sb2.AppendFormat("{0},{1},{2},{3},{4}", model.NameManager.UnmangleName(instanceObj.FullName).Replace(',', '`'),
                                paramValue.Key.Replace(',', '`'),
                                paramValue.Value.GetValue().ToString().Replace(',', '`').Replace(Environment.NewLine, " "),
                                paramValType.Name,
                                paramValue.Value.ParameterSpecializationType.ToString());

                            sb2.AppendLine();
                        }
                    }
                }

                File.WriteAllText(destination, sb2.ToString());
            }
        }
    }
}
